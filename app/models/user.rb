# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  image_url  :string(255)
#  uid        :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class User < ActiveRecord::Base
  has_many :votes, dependent: :destroy
  has_many :vote_options, through: :votes

  def voted_for?(poll)
    votes.any? {|v| v.vote_option.poll == poll}
  end

  has_secure_password
end