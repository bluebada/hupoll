class AddImageToVoteOptions < ActiveRecord::Migration
  def change
    add_column :vote_options, :image, :string
  end
end
