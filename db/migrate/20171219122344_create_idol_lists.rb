class CreateIdolLists < ActiveRecord::Migration
  def change
    create_table :idol_lists do |t|
      t.string :name
      t.string :image

      t.timestamps null: false
    end
  end
end
