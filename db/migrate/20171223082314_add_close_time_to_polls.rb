class AddCloseTimeToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :close_time, :datetime
  end
end
