class CreateUserSettings < ActiveRecord::Migration
  def change
    create_table :user_settings do |t|
      t.references :user, index: true, foreign_key: true
      t.string :ip, default: "0.0.0.0"
      t.boolean :is_ban, default: false
      t.boolean :is_admin, default: false

      t.timestamps null: false
    end
  end
end
